#ifndef APPSETTINGS_H
#define APPSETTINGS_H

namespace settings {

/**
 * Settings keys names
 */
namespace keys {

constexpr const char *instance_url = "config/endpoint"; /**< Instance URL. */
constexpr const char *sync_delay = "config/sync-delay"; /**< Delay before syncing local time records. */
/** Delay before removing synced time records. */
constexpr const char *sync_remove_delay = "config/sync-remove-delay";
/** Number of recent issues to display. */
constexpr const char *recent_issues_count = "config/recent-issues";
/** Number of recent projects to display. */
constexpr const char *recent_projects_count = "config/recent-projects";
/** Issue overdue notification time, minutes. */
constexpr const char *overdue_notif_time = "config/overdue-notification";
/** Suggest to update estimate on overdue notification click */
constexpr const char *overdue_notif_set_estimate = "config/overdue-notification-set-estimate";
/** Enable local cache */
constexpr const char *enable_cache = "config/enable-cache";


/** Toggle time tracking */
constexpr const char *toggle_tracking_shortcut = "shortcuts/toggle-tracking";
/** Toggle main application window */
constexpr const char *toggle_appwindow_shortcut = "shortcuts/toggle-app-window";
/** Focus projects selection */
constexpr const char *focus_projects_shortcut = "shortcuts/focus-projects";
/** Focus issues selection */
constexpr const char *focus_issues_shortcut = "shortcuts/focus-issues";

constexpr const char *active_project = "state/project"; /**< Last active project */
constexpr const char *last_issues_group = "last-issues"; /**< Last active issues (project-bound) */
constexpr const char *window_position = "state/ui/main-window/pos"; /**< Last window position */
constexpr const char *window_size = "state/ui/main-window/size"; /**< Last window size */
constexpr const char *time_rec_split = "state/ui/trec-split"; /**< Time records split position */
constexpr const char *time_rec_height = "state/ui/trec-expanded-height"; /**< Time records height */
/** List of recent projects */
constexpr const char *recent_projects_list = "state/recent-projects";

constexpr const char *max_menu_width = "ui/max-menu-width"; /**< Maximum width of drop-down menus */

constexpr const char *auth_token = "auth/token"; /**< Authentication token */
}

/**
 * Default settings values
 */
namespace defaults {

constexpr const char *instance_url = "https://gitlab.com"; /**< Default instance URL. */
constexpr int sync_delay = 60; /**< Delay before syncing local time records. */
constexpr int sync_remove_delay = 10; /**< Delay before removing synced time records. */
constexpr int recent_issues_count = 10; /**< Number of recent issues to display. */
constexpr int recent_projects_count = 10; /**< Number of recent projects to display. */
constexpr int overdue_notif_time = 15; /**< Notify this minutes before overdue. */
/** Suggest to update estimate on overdue notification click */
constexpr bool overdue_notif_set_estimate = false;
/** Suggest to update estimate on overdue notification click */
constexpr bool enable_cache = false;


constexpr const char *toggle_tracking_shortcut = "Ctrl+Alt+T"; /**< Toggle time tracking */
constexpr const char *toggle_appwindow_shortcut = "Ctrl+Alt+S"; /**< Toggle main application window */
constexpr const char *focus_projects_shortcut = "Ctrl+1"; /**< Focus projects selection */
constexpr const char *focus_issues_shortcut = "Ctrl+2"; /**< Focus issues selection */

constexpr int max_menu_width = 500; /**< Maximum width of drop-down menus. */
}

}
#endif // APPSETTINGS_H
