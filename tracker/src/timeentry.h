#ifndef TIMEENTRY_H
#define TIMEENTRY_H

#include <QDialog>
#include <QMenu>
#include <QVector>
#include <QSpinBox>
#include <QSettings>

namespace Ui {
class TimeEntry;
}

/**
 * Time entry dialog.
 */
class TimeEntry: public QDialog {
    Q_OBJECT

public:
    /**
     * Create TimeEntry dialog.
     * @param parent Parent widget.
     * @param settings_group Name of the settings group.
     */
    explicit TimeEntry(QWidget *parent=nullptr, QString settings_group="timeentry");
    ~TimeEntry();

    /**
     * Return current time set in the dialog.
     * @return
     */
    long time() const { return total_time; }

public slots:
    /**
     * Set time in dialog controls to the passed value.
     * @param seconds New time value.
     */
    void set_time(long seconds);

    /**
     * Set dialog header text.
     * @param text New text.
     */
    void set_header_text(const QString &text);

    /**
     * Set button icon and text
     * @param text Button text
     * @param icon Button icon
     */
    void set_button(const QString &text, const QIcon &icon);

    /**
     * Set project information.
     * Project info is shown on a separate line.
     * @param title Project information to display. Pass empty string to hide the line.
     * @param tooltip Tooltip text.
     */
    void set_project_info(const QString &title, const QString &tooltip);

private slots:
    /**
     * Update time controls toggle actions to not allow toggling the only time control.
     */
    void update_toggle_actions();

    /**
     * Handle update in the free text input string.
     * Also updates formatted time string at the bottom and cached time value.
     * @param value
     */
    void parse_time_string(const QString &value);

    /**
     * Handle value change in a time control.
     * This updates cached time value and formatted time string at the bottom.
     */
    void update_time_from_controls();

    /**
     * Toggle free text input mode.
     * Also transfers current time value between time enter modes.
     * @param free_mode True if free mode is enabled
     */
    void toggle_free_text(bool free_mode);

    /**
     * Handle spin box value was wrapped.
     * @param direction Wrap direction: +1 if wrapped from maximum() to minimum(), -1 otherwise.
     */
    void handle_wrap(int direction);

    /**
     * Update time string with the current time.
     */
    void update_current_time_string();

    /**
     * Save UI state in settings.
     */
    void save_ui_state();

    /**
     * Restore UI state from settings.
     */
    void restore_ui_state();

private:
    Ui::TimeEntry *ui; /**< UI instance */
    QMenu *settings_menu; /**< Popup menu with settings */
    /** Ordered list of time controls to handle wrapping */
    QVector<std::pair<QSpinBox *, QAction *>> time_controls;
    long total_time; /**< Total time */
    QString settings_group; /**< QSettings group name */
    QSettings settings; /**< Settings object */
};

#endif // TIMEENTRY_H
