#include <QFile>

#include <QTimer>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonArray>

#include "asyncjsonmodel.h"

AsyncJsonModel::AsyncJsonModel(gitlabclient::AsyncJsonObjList *objlist, QObject *parent):
    QAbstractTableModel(parent)
{
    set_objects_list(objlist);
}

AsyncJsonModel::AsyncJsonModel(const QStringList &columns, gitlabclient::AsyncJsonObjList *objlist,
                               QObject *parent):
    QAbstractTableModel(parent)
{
    set_objects_list(objlist);
    populate_columns(columns);
}


int AsyncJsonModel::rowCount(const QModelIndex &parent) const
{
    // For table models, only the root node (an invalid parent) should return the list's size.
    // For all other (valid) parents, rowCount() should return 0 so that it does not become
    // a tree model.
    if (parent.isValid() || !objects) {
        return 0;
    }

    return objects->fetched_results() + cache.size();
}

int AsyncJsonModel::columnCount(const QModelIndex &parent) const
{
    // For table models, only the root node (an invalid parent) should return the number of columns.
    // For all other (valid) parents, columnCount() should return 0 so that it does not become
    // a tree model.
    if (parent.isValid()) {
        return 0;
    }

    return _columns.size();
}

QVariant AsyncJsonModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !objects) {
        return QVariant();
    }
    const int fetched_count = objects->fetched_results();
    const int total_rows = fetched_count + cache.size();
    // Check bounds.
    int row = index.row();
    if (row < 0 || row >= total_rows) {
        return QVariant();
    }
    int column = index.column();
    if (column < 0 || column >= _columns.size()) {
        return QVariant();
    }

    const QJsonObject &obj = row < fetched_count ? objects->at(row): cache.at(row - fetched_count);

    switch (_columns[column].kind) {
        case ColumnInfo::Kind::Data:
            // Data columns, directly served from the JSON
            if (role == Qt::DisplayRole || role == Qt::EditRole || role == Qt::ToolTipRole) {
                return obj[_columns[column].name];
            }
            break;

        case ColumnInfo::Kind::Computed:
            return _computed_columns[_columns[column].index](obj, role);

        case ColumnInfo::Kind::Caching:
            return _caching_computed_columns[_columns[column].index](obj, role, columns_cache[row]);
    }
    return QVariant();
}

QVariant AsyncJsonModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    // Skip unsupported roles.
    if (role != Qt::DisplayRole && role != Qt::EditRole) {
        return QVariant();
    }

    // Bounds checking.
    if (section < 0) {
        return QVariant();
    }

    switch (orientation) {
        case Qt::Orientation::Vertical:
            // Safe to skip bounds checking.
            return QString::number(section);

        case Qt::Orientation::Horizontal:
            // Bounds checking.
            if (section >= _columns.size()) {
                return QVariant();
            }
            return titles[section];
    }
    return QVariant();
}

bool AsyncJsonModel::is_updating()
{
    return objects && objects->pending();
}

QStringList AsyncJsonModel::columns() const
{
    QStringList column_names;
    for (const ColumnInfo &info: qAsConst(_columns)) {
        column_names.push_back(info.name);
    }
    return column_names;
}

int AsyncJsonModel::add_computed_column(const QString &name,
                                        std::function<ComputeColumnFn> compute_fn)
{
    _columns.push_back({ColumnInfo::Kind::Computed, _computed_columns.size(), name});
    column_names_map[name] = _columns.size() - 1;
    _computed_columns.push_back(compute_fn);
    titles.push_back(name);
    return _columns.size() - 1;
}

int AsyncJsonModel::add_computed_column(
    const QString &name,
    std::function<AsyncJsonModel::CachingComputeColumnFn> compute_fn)
{
    _columns.push_back({ColumnInfo::Kind::Caching, _caching_computed_columns.size(), name});
    column_names_map[name] = _columns.size() - 1;
    _caching_computed_columns.push_back(compute_fn);
    titles.push_back(name);
    return _columns.size() - 1;
}

QModelIndex AsyncJsonModel::index(int row, const QString &column)
{
    int column_index = column_names_map.value(column, -1);
    if (column_index < 0) {
        return QModelIndex();
    }
    return index(row, column_index);
}

void AsyncJsonModel::refresh()
{
    if (!objects || objects->pending()) {
        return;
    }
    objects->refresh(true);
    beginResetModel();
    emit update_started();
    emit updating(true);
}

void AsyncJsonModel::data_loaded()
{
    qDebug() << "Loaded items:" << objects->fetched_results();
    columns_cache.clear();
    merge_preloaded_cache();
    save_cache(cache_path);
    endResetModel();
    emit update_finished();
    emit updating(false);
}

void AsyncJsonModel::populate_columns(const QStringList &columns)
{
    int idx = 0;
    for (const QString &cname: qAsConst(columns)) {
        column_names_map[cname] = idx++;
        _columns.push_back({ColumnInfo::Kind::Data, idx, cname});
        titles.push_back(cname);
    }
}

void AsyncJsonModel::set_objects_list(gitlabclient::AsyncJsonObjList *objlist)
{
    // Delete previous objects list, if we have it.
    if (objects) {
        objects->deleteLater();
    }

    if (!objlist) {
        return;
    }

    objects = objlist;

    // Reset will finish after fetching all projects.
    beginResetModel();
    objects->setParent(this);

    // Get notified when the list fetches all results.
    connect(objects, &gitlabclient::AsyncJsonObjList::loaded,
            this, &AsyncJsonModel::data_loaded);
    connect(objects, &gitlabclient::AsyncJsonObjList::failed,
            this, &AsyncJsonModel::data_loaded);
    // re-emit `failed` signal
    connect(objects, &gitlabclient::AsyncJsonObjList::failed,
            this, &AsyncJsonModel::failed);
    qDebug("Attached to the AsyncJsonObjList");
    objects->fetch_all();
    qDebug("FetchAll requested");
    emit update_started();
    emit updating(true);

    // If the passed list has already fetched all result it will not emit the signal.
    if (!objects->pending()) {
        data_loaded();
    }
}

void AsyncJsonModel::set_columns(const QStringList &columns)
{
    populate_columns(columns);
    _computed_columns.clear();
    _caching_computed_columns.clear();
    columns_cache.clear();
}

void AsyncJsonModel::set_column_title(int index, const QString &title)
{
    if (index < 0 || index >= titles.size()) {
        return;
    }
    titles[index] = title;
}

QDateTime AsyncJsonModel::set_cache_path(const QString &file_path)
{
    if (cache_key.isEmpty()) {
        qWarning() << "Ignoring attempt to set cache with no cache key set.";
        return QDateTime();
    }

    QDateTime cache_time = preload_cache(file_path);

    // If currently updating, the cache will take an effect as soon as the update finishes.
    if (is_updating())
        return cache_time;
    beginResetModel();
    merge_preloaded_cache();
    save_cache(file_path);
    endResetModel();
    return cache_time;
}

QDateTime AsyncJsonModel::preload_cache(const QString &file_path)
{
    // Cache doesn't work without cache key.
    if (cache_key.isEmpty()) {
        qWarning() << "Ignoring attempt to preload cache with no cache key set.";
        return QDateTime();
    }
    cache_path = file_path;
    QDateTime cache_time = read_cache_file(file_path);
    return cache_time;
}

QDateTime AsyncJsonModel::read_cache_file(const QString &path)
{
    qDebug() << "Loading cache from:" << path;
    QFile cache_file(path);

    if (!cache_file.open(QIODevice::ReadOnly)) {
        qDebug() << "Couldn't load cache file:" << path;
        return QDateTime();
    }

    QByteArray cache_data = cache_file.readAll();

    const QJsonObject cache_doc(QJsonDocument::fromJson(cache_data).object());
    QDateTime cache_time = QDateTime::fromString(cache_doc["when"].toString(), Qt::ISODate);
    qDebug() << "Cache time:" << cache_time;
    const QJsonArray &cached_objects = cache_doc["objects"].toArray();
    qDebug() << "Cache items:" << cached_objects.size();
    preloaded_cache.clear();
    preloaded_cache.reserve(cached_objects.size());
    for (const QJsonValue& value: cached_objects){
        preloaded_cache.append(value.toObject());
    }

    return cache_time;
}

void AsyncJsonModel::merge_preloaded_cache()
{
    cache.clear();
    // Nothing to do.
    if (!objects || cache_key.isEmpty() || !preloaded_cache.size()) {
        return;
    }
    QSet<QString> keys;
    // Populate list of all loaded keys to skip them while merging.
    for (int i = 0, len = objects->fetched_results(); i < len; ++i) {
        keys.insert(objects->at(i)[cache_key].toString());
    }
    for (const QJsonObject &obj: preloaded_cache) {
        if (keys.contains(obj[cache_key].toString())) {
            continue;
        }
        cache.append(obj);
    }
}

void AsyncJsonModel::save_cache(const QString &path)
{
    // Nothing to do.
    if (path.isEmpty()) {
        return;
    }
    if (!objects) {
        qWarning() << "Ignoring attempt to save cache with no objects.";
        return;
    }

    qDebug() << "Saving cache to:" << path;

    QFile cache_file(path);

    if (!cache_file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        qWarning() << "Couldn't create cache file:" << path;
        return;
    }

    QJsonObject cache_obj;
    cache_obj["when"] = objects->response_time().toString(Qt::ISODate);
    QJsonArray merged_objects;
    for (int i = 0, len = objects->fetched_results(); i < len; ++i) {
        merged_objects.append(objects->at(i));
    }
    for (const QJsonObject &obj: cache) {
        merged_objects.append(obj);
    }

    cache_obj["objects"] = merged_objects;
    cache_file.write(QJsonDocument{cache_obj}.toJson());
}
