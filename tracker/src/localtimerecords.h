#ifndef LOCALTIMERECORDS_H
#define LOCALTIMERECORDS_H

#include <QAbstractTableModel>
#include <QDateTime>
#include <QTimer>
#include <QVector>
#include <QHash>

#include <gitlabclient/gitlabclient>

#include "asyncjsonmodel.h"
#include "asyncprojectsmodel.h"

/**
 * Stores local time records and syncs them.
 * Implements QAbstractTableModel so can be used to display local records and their status.
 * Table model has next columns:
 * - Entity: Issue or Merge request.
 * - Id: Id of the corresponding entity.
 * - Time: Time record.
 * - Status: Sync status
 */
class LocalTimeRecords: public QAbstractTableModel {
    Q_OBJECT

public:
    /** Possible statuses of time records. */
    enum class RecordStatus {
        Local, /**< Local-only, not attempted to sync. */
        Sent,  /**< Sync started: request sent but not confirmed. */
        Synced, /**< Record is synced (confirmed). */
        Error /**< The last sync attempt failed */
    };
    /** Type of entity the time recorded for. */
    enum class Entity {
        Issue, /**< Entity is issue */
        MergeRequest /**< Entity is merge request */
    };
    /** Columns supported by the model.
     * Wrapped in a struct make it both scoped and int-convertible. */
    struct Columns {
        enum {
            PROJECT, /**< Project */
            ENTITY, /**< Entity column */
            ID, /**< Id column */
            PROJECT_ENTITY, /**< Project and entity and type */
            TIME, /**< Tracked time column */
            STATUS, /**< Record status */
            NUM_COLUMNS /**< Number of supported columns, must be the last one */
        };
    };
    /** Item roles supported by the model.
     * Wrapped in a struct make it both scoped and int-convertible. */
    struct Roles {
        enum {
            ENTITY = Qt::UserRole, /**< Entity role, int */
            ID, /**< Id role, int */
            TIME_SECONDS, /**< Time, seconds, int */
            RECORD_STATUS /**< Record status, int */
        };
    };

    /** Construct local time records */
    explicit LocalTimeRecords(QObject *parent=nullptr);

    /**
     * Provide header information.
     * @param section Section number.
     * @param orientation Orientation.
     * @param role Data role.
     * @return Header data.
     */
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role=Qt::DisplayRole) const override;

    /**
     * Returns count of rows in the model.
     * @param parent Parent index.
     * @return row count.
     */
    int rowCount(const QModelIndex &parent=QModelIndex()) const override;

    /**
     * Returns count of columns in the model.
     * @param parent Parent index.
     * @return column count.
     */
    int columnCount(const QModelIndex &parent=QModelIndex()) const override;

    /**
     * Returns model data.
     * @param index Data index.
     * @param role Data role.
     * @return Model data.
     */
    QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const override;

    /**
     * Return auto remove timeout (seconds).
     * Synced records are automatically removed from the model after this timeout.
     * @return Auto remove timeout value, seconds.
     */

    int auto_remove_timeout() const { return auto_remove_timeout_sec; }
    /**
     * Set auto remove timeout.
     * Synced records are automatically removed from the model after this timeout.
     * @param seconds Timeout value, seconds.
     */
    void set_auto_remove_timeout(int seconds);

    /**
     * Returns auto sync timeout (seconds).
     * Records are automatically synced within this interval.
     * @return Auto sync interval, seconds.
     */
    int auto_sync_period() const { return auto_sync_period_sec; }

    /**
     * Set auto sync interval (seconds).
     * Records are automatically synced within this interval.
     * @param seconds Timeout value, seconds.
     */
    void set_auto_sync_period(int seconds);

    /**
     * Returns gitlab client pointer.
     * @return Gitlab client pointer.
     */
    gitlabclient::Client *gitlab_client() const { return client; }

    /**
     * Set gitlab client pointer.
     * @param value Pointer to a gitlab client instance.
     */
    void set_gitlab_client(gitlabclient::Client *value) { client = value; }

    /**
     * Returns total time tracked for the given project and entity and not synced to the server.
     * @param project_id Project id
     * @param entity Type of the entity: issue or merge request
     * @param entity_id Entity id
     * @return Local tracked time, seconds.
     */
    int get_local_tracked_time(long project_id, Entity entity, long entity_id);

    /**
     * Set projects model.
     * If projects model is set, it is used to query project name and return it instead of ID
     * in project column display and tooltip roles.
     * @param model Projects model.
     */
    void set_projects_model(AsyncProjectsModel *model) { projects_model = model; }

public slots:
    /**
     * Add record to the model.
     * @param project Project id
     * @param type Record (entry) type.
     * @param id Entity id (issue / mr iid).
     * @param time_seconds Time in seconds.
     */
    void add_record(long project, Entity type, long id, long time_seconds);

    /**
     * Sync records.
     */
    void sync();

    /**
     * Remove a record with the given index.
     * @param record_index Row index.
     */
    void remove_record(int record_index);

    /**
     * Remove a record given a persistent model index.
     * @overload
     * @param index Record index. The whole record is removed (column is ignored).
     */
    void remove_record(QPersistentModelIndex index);

    /**
     * Mark record with the given index to be synced on next update event.
     * Records with state other than Local are skipped.
     * @param record_index Row index.
     */
    void sync_record(int record_index);

    /**
     * Toggle TTS timer state to be paused on running.
     * When paused, TTS time of records will not decrease and no records will be synced.
     * @param run True to start timer or false to pause it.
     */
    void toggle_tts_timer(bool run);

    /**
     * Convenient function that calls `toggle_tts_timer(true)`.
     */
    void stop_tts_timer() { toggle_tts_timer(false); }

    /**
     * Convenient function that calls `toggle_tts_timer(false)`.
     */
    void start_tts_time() { toggle_tts_timer(true); }

signals:
    /**
     * Synced signal is emitted each time a record is sent to the remote
     * @param project Project ID.
     * @param type Entity type (MR / Issue).
     * @param id Entity ID.
     * @param total_spent Total spent time as returned by the server.
     * @param remote_time Remote (server) time of the response.
     */
    void synced(long project, Entity type, long id, long total_spent, QDateTime remote_time);

private:
    /** Time record */
    struct TimeRecord {
        RecordStatus status; /**< Record status */
        unsigned int tts; /**< Time to sync */
        long tracked_time_sec; /**< Recorded time, seconds */
        Entity entity; /**< Type of the entity the time tracked for: Issue of MR */
        long entity_id; /**< Issue or MR id */
        long project_id; /**< ID of the project */
    };

    /**
     * Update record information after it was synced.
     * @param row_index Record (row) index.
     */
    void update_synced_record(int row_index);

    /**
     * Update record information after it failed to sync.
     * @param row_index Record (row) index.
     * @param error Network error occurred during the sync.
     */
    void update_failed_record(int row_index, QNetworkReply::NetworkError error);

    QVector<TimeRecord> records; /**< Stored records */
    unsigned int auto_remove_timeout_sec = 10; /**< Timeout to remove synced records */
    unsigned int auto_sync_period_sec = 60; /**< Auto sync period, seconds */
    QHash<int, QString> column_names; /**< Column names (header) */
    QTimer auto_sync_timer; /**< Auto sync timer */
    gitlabclient::Client *client=nullptr; /**< GitLab client */
    AsyncProjectsModel *projects_model=nullptr; /**< Projects model */
};


#endif // LOCALTIMERECORDS_H
