#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QAbstractButton>
#include <QDialog>
#include <QSettings>

namespace Ui {
class SettingsDialog;
}

/**
 * Application settings dialog.
 */
class SettingsDialog: public QDialog {
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent=nullptr);
    ~SettingsDialog() override;

public slots:
    /**
     * Overloaded function calls `load_settings` just before calling base implementation.
     * @return Value returned by base implementation.
     * @overload
     */
    virtual int exec() override;

    /**
     * Overloaded function calls `load_settings` just before calling base implementation.
     * @overload
     */
    virtual void open() override;

    /**
     * Load application settings to UI.
     */
    void load_settings();

    /**
     * Save current dialog values to application settings.
     */
    void save_settings();

    /**
     * Reset UI values to their defaults.
     */
    void reset();

private slots:
    /**
     * Handle click of a button from QDialogButtonBox
     * @param button Clicked button.
     */
    void handle_buttonbox_button(QAbstractButton *button);

    /**
     * Clear application caches
     */
    void clear_caches();

private:
    Ui::SettingsDialog *ui;
    QSettings *settings;
};

#endif // SETTINGSDIALOG_H
