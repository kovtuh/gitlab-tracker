if [ "$#" -ne 3 ]; then
    echo "Usage: $0 <release-description> <asset-name> <asset-url>"
    exit 1
fi

# Publish release
wget --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" \
     --post-data "$(jq -n --arg name "$CI_COMMIT_TAG" --arg description "$1" --arg aname "$2" --arg aurl "$3" '{name: $name, tag_name: $name, description: $description, assets: {links: [{name: $aname, url: $aurl}]}}')" \
     "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/releases"
