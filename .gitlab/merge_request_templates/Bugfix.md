## Bug details
<!-- Briefly describe what this MR is about -->

## Related issues
Resolves #XXX.

## Checklist
- [ ] MR title includes issue action if relevant ("resolves #xxx").
- [ ] Source branch is `bugfix/*`.
- [ ] Target branch is `develop` or `release/*`.
- [ ] Contains isolated set of changes related to the bugfix.
- [ ] Changes are described in the changelog (unreleased section).

## Post-merge actions
- [ ] Move issue to the ~Merging column.


/label ~Bug
/target_branch develop

